
import plotly.graph_objects as go
import dash_bootstrap_components as dbc
import requests
from jupyter_dash import JupyterDash
#from pyngrok import ngrok
#import dash_table as dt

from dash import Dash, html, dcc, html, Input, Output, dash_table as dt
import plotly.express as px
import pandas as pd
from pandas import json_normalize

import random
import time
import datetime
from time import strftime
import dash_daq as daq
# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

################################################################################################# LINE

#response_token = requests.get("https://line-notify-scouter-key-default-rtdb.firebaseio.com/token.json")

#print(response_token.json())
#token = response_token.json()
#token = '2Ok2kPUjwAjXpwdvTcfanT7a04ghbJQ7UQTkk9mDfjC'
#headers = {'content-type':'application/x-www-form-urlencoded','Authorization':'Bearer '+ token}
#################################################################################################

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
#external_stylesheets = [dbc.themes.QUARTZ]
app = Dash(__name__, external_stylesheets=external_stylesheets)
#app = JupyterDash(__name__)
colors = {
    'background': '#111111',
    'text': '#7FDBFF'
}



response = requests.get("https://scouter-optical-data-default-rtdb.firebaseio.com/current.json")


df_1 = json_normalize(response.json(),max_level=0)

#df = json_normalize(response.json()["XXX"])
#print(df.alarm.to_list()[0])

rows_1 = []
for i in df_1.columns:
  rows_1.append(i)
  print(rows_1,type(rows_1))

app.layout = html.Div([



    html.Div(style={'backgroundColor': 'rgb(230, 230, 230)'}, children=[
    html.H1(
        children='SCOUTER >> Fiber Monitoring System',
        style={
            'textAlign': 'left',
            #'color': colors['text']
            'color': 'rgb(102, 0, 102)'
        }
    ),
    html.Br(),

    dcc.Dropdown(rows_1, rows_1[0], id='demo-dropdown', placeholder="Select Site")
    ]),

    html.Div(style={'backgroundColor': 'rgb(230, 230, 230)'}, children=[
    html.H2(children='map'),
    dcc.Graph(id='map')
    ]),

    html.Div(style={'display': 'flex', 'flex-direction': 'row'}, children=[



    html.Div(style={'padding': 10, 'flex': 1}, children=[


    daq.LEDDisplay(
        id='tx',
        label="Transmit Optical Power (Tx)",
        value=0,
        size=60,
        color="black"
    ),
    daq.LEDDisplay(
        id='rx',
        label="Receive Optical Power (Rx) ",
        value=0,
        size=60,
        color="black"
    ),


    dcc.Interval(
            id='interval-component',
            interval=15*1000, # in milliseconds
            n_intervals=0
        )
    ])
    ]),
    html.Div(style={'backgroundColor': 'rgb(230, 230, 230)'}, children=[
    html.H2(children='Current Alarm',),
    html.H3(id='alarm')
    ]),
    html.Div(style={'backgroundColor': 'rgb(230, 230, 230)'}, children=[
    html.H2(children='Historical Data'),
    dcc.Graph(id='graph')
    ])

])


@app.callback(



    Output(component_id='tx', component_property='value'),
    Output(component_id='rx', component_property='value'),
    Output(component_id='alarm', component_property='children'),

    Input('demo-dropdown', 'value'),
    Input('interval-component', 'n_intervals')
)
def update_output_div(value,n_intervals):

    response = requests.get("https://scouter-optical-data-default-rtdb.firebaseio.com/current.json")
    df = json_normalize(response.json()[value])

    if df.rx.to_list()[0] == -40.0:
      alarm = "Fiber Optic Loss"
    else:
      alarm = "No Alarm"

    return df.tx.to_list()[0], df.rx.to_list()[0], alarm

@app.callback(

    Output(component_id='graph', component_property='figure'),
    Input('demo-dropdown', 'value')

)
def update_output_div2(val):

    response = requests.get("https://scouter-optical-data-default-rtdb.firebaseio.com/log/"+val+".json")

    df = json_normalize(response.json(),max_level=0)
    df3 = pd.DataFrame(data={})
    rows = []
    for i in df:

      rows.append([df[i][0]['time'], float(df[i][0]['tx']) ,float(df[i][0]['rx'])])

    fig = go.Figure()
    df3 = pd.DataFrame(rows, columns=["time", "tx", "rx"] )

    fig.add_trace(go.Scatter(x=df3.time, y=df3.tx,
                mode='lines+markers',
                name='tx'))
    fig.add_trace(go.Scatter(x=df3.time, y=df3.rx,
                mode='lines+markers',
                name='rx'))

    return fig

@app.callback(

    Output(component_id='map', component_property='figure'),
    Input('demo-dropdown', 'value')

)
def update_output_div3(value):

    response = requests.get("https://scouter-optical-data-default-rtdb.firebaseio.com/"+value+".json")
    df = json_normalize(response.json(),max_level=0)

    lon = df.lon[0].split(",")
    lat = df.lat[0].split(",")

    fig = go.Figure(go.Scattermapbox(
      mode = "markers+lines",
      text = df.text[0],
      lon = df.lon[0].split(","),
      lat = df.lat[0].split(","),
      marker = {'size': 10,'color':'green'}))


    fig.update_layout(
      margin ={'l':0,'t':0,'b':0,'r':0},
      mapbox = {
        #'center': {'lon': lon[1], 'lat': lat[1]},
        'style' : 'open-street-map',
        'center': {'lon': float(lon[0]), 'lat': float(lat[0])},
        'zoom': 9})

    return fig

if __name__ == '__main__':
    app.run(debug=True)